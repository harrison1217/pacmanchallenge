﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PACManChallenge
{
    public class Program
    {
        static void Main(string[] args)
        {
            double points = 5000;
            int life = 3;
            int vgnum = 1;
            int counter = 1;

            string doc = System.IO.File.ReadAllText(@"C:\Users\Harrison Ball\Desktop\1150 .NET Class\Course Work\Projects\PACManChallenge\PACManChallenge\pacman-sequence.txt");
            string[] words = doc.Split(',');
            foreach (var word in words)
            {
                switch (word)
                {
                    case "Dot":
                        points = points + 10;
                        Console.WriteLine("Dot...10 points...Hit Enter");
                        Console.ReadLine();
                        break;                  
                    case "InvincibleGhost":
                        life = life - 1;
                        Console.WriteLine("You Hit an Invincible Ghost...Minus 1 Life. Lifes left:" + life + "...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Melon":
                        points = points + 1000;
                        Console.WriteLine("Melon!!!...1000 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Galaxian":
                        points = points + 2000;
                        Console.WriteLine("Galaxian...2000 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "VulnerableGhost":
                        points = (200 * vgnum) + points;
                        int temp = 200 * vgnum;
                        vgnum = vgnum + 1;
                        Console.WriteLine("You ate a Ghost..." + temp +"points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Bell":
                        points = points + 3000;
                        Console.WriteLine("You got a Bell...3000 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Cherry":
                        points = points + 100;
                        Console.WriteLine("Yum Cherry...100 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Strawberry":
                        points = points + 300;
                        Console.WriteLine("Yum Strawberry...300 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Orange":
                        points = points + 500;
                        Console.WriteLine("Orange!!...500 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Apple":
                        points = points + 700;
                        Console.WriteLine("A is for Apple...700 points...Hit Enter");
                        Console.ReadLine();
                        break;
                    case "Key":
                        points = points + 500;
                        Console.WriteLine("You found the Key...500 points...Hit Enter");
                        Console.ReadLine();
                        break;
                }
                
                if (life == 0)
                {
                    Console.WriteLine("GAME OVER!!! Wah KA WAh KA WAH KA ");
                    Console.ReadLine();
                }
                if (counter == 1)
                {
                    if (points >= 10000)
                    {
                        life = life + 1;
                        Console.WriteLine("You got an extra Life...Hit Enter");
                        Console.ReadLine();
                        counter = counter++;
                    }
                }   
                else if (counter == 2)
                {
                    if (points >= 20000)
                    {
                        life = life + 1;
                        Console.WriteLine("You got an extra Life...Hit Enter");
                        Console.ReadLine();
                        counter = counter++;

                    }
                }
                else if (counter == 3)
                {
                    if (points >= 30000)
                    {
                        life = life + 1;
                        Console.WriteLine("You got an extra Life...Hit Enter");
                        Console.ReadLine();
                        counter = counter++;
                    }
                }

            }
            Console.WriteLine("Your Total Points for the Game: " + points);
            Console.ReadLine();
        }
    }
}
